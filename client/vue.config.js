const path = require('path');

module.exports = {
    //build assets to server folder (we dont need deploy client folder)
    outputDir: path.resolve(__dirname, '../server/public'),
    // devServer: {
    //     proxy: {
    //         '/graphql': {
    //             target: 'http://localhost:4000'
    //         }
    //     }
    // },
    css: {
        loaderOptions: {
            sass: {
                additionalData: `
                    @import "@/scss/variables";
                    @import "@/scss/btns";
                    @import "@/scss/bootstrap";`
            }
        }
    }
};