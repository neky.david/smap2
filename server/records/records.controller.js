const RecordModel = require('./records.model');
const DetectService = require('../services/detect.service')

exports.insert = (req, res) => {
    DetectService.detect(req.body.img)
    res.status(201).send({ id: "TEST" });

    /*RecordModel.createRecord(req.body)
        .then((result) => {
            res.status(201).send({ id: result._id });
        });*/
};

exports.list = (req, res) => {
    let limit = req.query.limit && req.query.limit <= 100 ? parseInt(req.query.limit) : 10;
    let page = 0;
    if (req.query) {
        if (req.query.page) {
            req.query.page = parseInt(req.query.page);
            page = Number.isInteger(req.query.page) ? req.query.page : 0;
        }
    }
    RecordModel.list(limit, page)
        .then((result) => {
            res.status(200).send(result);
        })
};

exports.getById = (req, res) => {
    RecordModel.findById(req.params.recordId)
        .then((result) => {
            res.status(200).send(result);
        });
};

exports.updateById = (req, res) => {
    RecordModel.updateRecord(req.params.recordId, req.body)
        .then((result) => {
            res.status(204).send({});
        });
};

exports.removeById = (req, res) => {
    RecordModel.removeById(req.params.recordId)
        .then((result) => {
            res.status(204).send({});
        });
};