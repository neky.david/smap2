import axios from "axios"

var port = process.env.PORT || 3000;
var url = "http://localhost:" + port + "/records/";

export default {
    async getRecords(limit, page) {
        let res = await axios.get(url, {
            params: {
                limit: limit,
                page: page
            }
        });
        return res.data;
    },
    async getRecordById(recordId) {
        let res = await axios.get(url + recordId);
        return res.data;
    },
    async insertRecord(record) {
        console.log("INSERT:");
        console.log(record);
        console.log(url);
        let res = await axios.post(url, record, {
            headers: {
                "Content-Type": "application/json"
            }
        });
        return res.data;
    },
    async updateRecord(record) {
        let res = await axios.patch(url + record.id, record, {
            headers: {
                "Content-Type": "application/json"
            }
        });
        return res.data;
    },
    async deleteRecord(recordId) {
        let res = await axios.delete(url + recordId);
        return res.data;
    },
}