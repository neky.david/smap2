const RecordsController = require('./records.controller');

exports.routesConfig = function (app) {
    app.post('/records', [
        RecordsController.insert
    ]);
    app.get('/records', [
        RecordsController.list
    ]);
    app.get('/records/:recordId', [
        RecordsController.getById
    ]);
    app.patch('/records/:recordId', [
        RecordsController.updateById
    ]);
    app.delete('/records/:recordId', [
        RecordsController.removeById
    ]);
};
