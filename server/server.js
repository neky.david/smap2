const express = require('express');
const bodyParser = require('body-parser');
const RecordsRouter = require('./records/records.routes');

const app = express();

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    //res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE');
    res.header('Access-Control-Expose-Headers', 'Content-Length');
    res.header('Access-Control-Allow-Headers', 'Accept, Authorization, Content-Type, X-Requested-With, Range');
    if (req.method === 'OPTIONS') {
        return res.sendStatus(200);
    } else {
        return next();
    }
});

app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb', extended: true })); //parameterLimit: 50000

RecordsRouter.routesConfig(app);


port = process.env.PORT || 3000;

app.listen(port, function () {
    console.log('REST API listening at port %s', port);
});

//console.log("AAAA");