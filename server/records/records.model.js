const mongoose = require('../services/mongoose.service').mongoose;
const Schema = mongoose.Schema;

const recordSchema = new Schema({
    name: String,
    img:
    {
        data: Buffer,
        name: String,
        contentType: String
    },
    predictedData: Buffer
});

recordSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

// Ensure virtual fields are serialised.
recordSchema.set('toJSON', {
    virtuals: true
});

recordSchema.findById = function (cb) {
    return this.model('Record').find({ id: this.id }, cb);
};


const Record = mongoose.model('Record', recordSchema);

exports.findById = (id) => {
    return Record.findById(id)
        .then((result) => {
            if (result) {
                result = result.toJSON();
                delete result._id;
                delete result.__v;
            }

            return result;
        });
};

exports.createRecord = (recordData) => {
    const record = new Record(recordData);
    return record.save();
};

exports.list = (perPage, page) => {
    return new Promise((resolve, reject) => {
        Record.find()
            .limit(perPage)
            .skip(perPage * page)
            .exec(function (err, records) {
                if (err) {
                    reject(err);
                } else {
                    resolve(records);
                }
            })
    });
};

exports.updateRecord = (id, recordData) => {
    return Record.findOneAndUpdate({
        _id: id
    }, recordData);
};

exports.removeById = (recordId) => {
    return new Promise((resolve, reject) => {
        Record.deleteMany({ _id: recordId }, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(err);
            }
        });
    });
};

